import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import App from './App';
import { Alert } from 'react-native';

jest.mock('@react-native-async-storage/async-storage', () => {
  return {
    setItem: jest.fn(),
    getItem: jest.fn(),
    removeItem: jest.fn(),
    clear: jest.fn(),
  };
});

describe('App', () => {
  it('adds an item to the todo list', () => {
    const { getByPlaceholderText, getByText } = render(<App />);
    const input = getByPlaceholderText('Add item');
    const addBtn = getByText('Add Item');

    fireEvent.changeText(input, 'Some text');
    fireEvent.press(addBtn);

    expect(getByText('Some text')).toBeTruthy();
  });

  it('shows alert on remove item icon click', async () => {
    const { getByText, getByPlaceholderText } = render(<App />);

    const input = getByPlaceholderText('Add item');
    const addBtn = getByText('Add Item');

    fireEvent.changeText(input, 'Some text');
    fireEvent.press(addBtn);

    const removeBtn = getByText('X');

    Alert.alert = jest.fn();
    fireEvent.press(removeBtn);
    expect(Alert.alert.mock.calls.length).toBe(1);
  });

  it('shows alert on add item click when no value is passed to input', async () => {
    const { getByPlaceholderText, getByText } = render(<App />);
    const input = getByPlaceholderText('Add item');
    const addBtn = getByText('Add Item');
    Alert.alert = jest.fn();

    fireEvent.changeText(input, '');
    fireEvent.press(addBtn);

    expect(Alert.alert.mock.calls.length).toBe(1);
  });

  it('shows alert on add item click when there already is item with passed title', async () => {
    const { getByPlaceholderText, getByText } = render(<App />);
    const input = getByPlaceholderText('Add item');
    const addBtn = getByText('Add Item');
    Alert.alert = jest.fn();

    fireEvent.changeText(input, 'test');
    fireEvent.press(addBtn);

    fireEvent.changeText(input, 'test');
    fireEvent.press(addBtn);

    expect(Alert.alert.mock.calls.length).toBe(1);
  });
});

import { useState, useEffect, useCallback } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const useAsyncStorageState = (id, defaultValue = null) => {
  const [value, setValue] = useState(defaultValue);

  const getValueFromStorage = useCallback(async () => {
    const data = await AsyncStorage.getItem(id);
    const item = JSON.parse(data) || { value: defaultValue };
    console.debug(`Read ${item.value} for ${id}`);

    setValue(item.value);
  }, [id, defaultValue]);

  const setter = useCallback(
    async newValue => {
      setValue(newValue);
      await AsyncStorage.setItem(id, JSON.stringify({ value: newValue }));
      console.debug(`Saved ${newValue} for ${id}`);
    },
    [id],
  );

  useEffect(() => {
    getValueFromStorage();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return [value, setter, getValueFromStorage];
};

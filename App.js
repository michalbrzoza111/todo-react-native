import React, { useState } from 'react';
import { Text, ScrollView, Button, TextInput, Alert } from 'react-native';
import styled from 'styled-components/native';
import { useAsyncStorageState } from './hooks/useAsyncStorageState';

const App = () => {
  const [todoItems, setTodoItems] = useAsyncStorageState('todo-app', []);

  const [newItem, setNewItem] = useState('');

  const addItem = () => {
    if (!newItem) {
      Alert.alert('Error', 'Please enter a valid item');
      return;
    }
    if (todoItems?.includes(newItem)) {
      Alert.alert('Error', 'Item already exists');
      return;
    }
    setTodoItems([...todoItems, newItem]);
    setNewItem('');
  };

  const removeItem = item => {
    Alert.alert(
      'Confirm',
      'Are you sure you want to remove this item?',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            setTodoItems(todoItems.filter(i => i !== item));
          },
        },
      ],
      { cancelable: false },
    );
  };

  const list = todoItems?.length > 0 ? todoItems : [];

  return (
    <Container>
      <ScrollView>
        {list?.map(item => (
          <TodoItem key={item}>
            <Text>{item}</Text>
            <Button title="X" onPress={() => removeItem(item)} />
          </TodoItem>
        ))}
      </ScrollView>
      <InputContainer>
        <TextInput
          value={newItem}
          onChangeText={setNewItem}
          placeholder="Add item"
        />
        <Button title="Add Item" onPress={addItem} />
      </InputContainer>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  padding: 20px;
  margin-top: 20px;
`;

const TodoItem = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 10px;
`;

const InputContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 20px;
`;

export default App;
